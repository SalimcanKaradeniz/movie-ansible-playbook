- hosts: api_servers
  user: root
  vars:
    dt: "{{ ansible_date_time.iso8601_basic_short }}"
    app_path: "/usr/local/app"

  tasks:
    - name: Activate IP Contrack Module
      modprobe:
        name: ip_contrack
        state: present

    - name: Tweak system
      sysctl:
        name: "{{ item.name }}"
        value: "{{ item.val }}"
        sysctl_set: yes
        state: present
        reload: yes
      with_items:
        - {name: "net.netfilter.nf_conntrack_max", val: "3145728"}
        - {name: "net.netfilter.nf_conntrack_tcp_timeout_syn_recv", val: "40"}
        - {name: "net.ipv4.tcp_max_orphans", val: "262144"}
        - {name: "net.ipv4.tcp_mem", val: "1048576 1572864 4194304"}
        - {name: "net.ipv4.neigh.default.gc_thresh1", val: "4096"}
        - {name: "net.ipv4.neigh.default.gc_thresh2", val: "8192"}
        - {name: "net.ipv4.neigh.default.gc_thresh3", val: "16384"}
        - {name: "net.ipv4.tcp_tw_reuse", val: "1"}
        - {name: "net.ipv4.ip_forward", val: "0"}
        - {name: "net.ipv4.tcp_syncookies", val: "1"}
        - {name: "net.core.somaxconn", val: "10240"}

    - name: Set PAM Limits
      pam_limits:
        domain: "{{ item.user }}"
        limit_type: "{{ item.type }}"
        limit_item: "{{ item.name }}"
        value: "{{ item.val }}"
      with_items:
        - {user: "www-data", type: "soft", name: "nofile", val: "65535"}
        - {user: "www-data", type: "hard", name: "nofile", val: "65535"}
        - {user: "root", type: "soft", name: "nofile", val: "65535"}
        - {user: "root", type: "hard", name: "nofile", val: "65535"}
        - {user: "www-data", type: "soft", name: "nproc", val: "65535"}
        - {user: "www-data", type: "hard", name: "nproc", val: "65535"}
        - {user: "root", type: "soft", name: "nproc", val: "65535"}
        - {user: "root", type: "hard", name: "nproc", val: "65535"}

    - name: Install a list of packages
      apt:
        name: "{{ packages }}"
        state: present
      vars:
        packages:
        - supervisor
        - libcurl4
        - libcurl4-openssl-dev
        - libssl-dev
        - python-virtualenv
        - virtualenv
        - git
        - python-dev
        - pypy
        - pypy-dev
        - pypy-lib
        - build-essential
        - gcc
        - libpq-dev
        - nginx-full

    - name: Create virtualenv
      pip:
        name: pip
        virtualenv: "{{ app_path }}/venv"
        virtualenv_python: pypy

    - name: Create deployment directory structure
      file:
        path: "{{ app_path }}/{{ item }}"
        state: directory
      with_items:
        - "archives"
        - "deployments"

    - name: Checkout code
      git:
        repo: "{{ REPO_URL }}"
        dest: "{{ app_path }}/deployments/{{ dt }}/"
        archive: "{{ app_path }}/archives/{{ dt }}.zip"
        ssh_opts: "-o StrictHostKeyChecking=no"

    - name: Install requirements
      pip:
        requirements: "{{ app_path }}/deployments/{{ dt }}/requirements.txt"
        virtualenv: "{{ app_path }}/venv"

    - name: Migrate DB
      shell: "PYTHONPATH=. DB_CONNECTION_STRING={{ DB_CONNECTION_STRING }} {{ app_path }}/venv/bin/alembic upgrade head"
      args:
        chdir: "{{ app_path }}/deployments/{{ dt }}"

    - name: Delete .git directory
      file:
        path: "{{ app_path }}/deployments/{{ dt }}/.git"
        state: absent

    - name: Remove old codebase softlink
      file:
        path: "{{ app_path }}/running"
        state: absent

    - name: Create new codebase softlink
      file:
        src: "{{ app_path }}/deployments/{{ dt }}"
        path: "{{ app_path }}/running"
        state: link

    - name: Copy supervisord config files
      template:
        src: "templates/api.conf.j2"
        dest: "/etc/supervisor/conf.d/api.conf"

    - file:
        path: /usr/local/app
        owner: www-data
        group: www-data
        mode: 0755
        recurse: yes

    - name: Recognize api program by supervisor
      supervisorctl:
        name: "api"
        state: present

    - name: Restart api program with supervisor
      supervisorctl:
        name: "api"
        state: restarted

    - name: Copy nginx config file
      template:
        src: "templates/api.nginx.conf.j2"
        dest: "/etc/nginx/nginx.conf"

    - name: Copy nginx site config file
      template:
        src: "templates/api.nginx.site.conf"
        dest: "/etc/nginx/sites-available/default"

    - name: Restart Nginx
      become: true
      service:
        name: nginx
        state: restarted
